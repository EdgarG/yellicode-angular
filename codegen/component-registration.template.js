"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const core_1 = require("@yellicode/core");
const templating_1 = require("@yellicode/templating");
const typescript_1 = require("@yellicode/typescript");
const angular_1 = require("@yellicode/angular");
const fileName = path.join("../src/app", "app.component-registration.ts");
templating_1.Generator.generateFromModel({ outputFile: fileName }, (writer, model) => {
    const tsw = new typescript_1.TypeScriptWriter(writer);
    const componentNames = model.components.map((c) => c.name);
    componentNames.push('AppSideNav'); // Add our custom side navigation
    // Write imports
    componentNames.forEach((name) => {
        const componentName = core_1.NameUtility.camelToKebabCase(name);
        tsw.writeImports(`./components/${componentName}/${componentName}.component`, [`${name}Component`]);
    });
    tsw.writeLine();
    // Declarations
    tsw.writeLine("export const declarations = [");
    tsw.increaseIndent();
    tsw.writeLines(componentNames.map((name) => `${name}Component`), ",");
    tsw.decreaseIndent();
    tsw.writeLine("]");
    // Routes
    tsw.writeLine("export const routes = [");
    tsw.increaseIndent();
    model.components
        .filter((c) => c.createRoute)
        .forEach((c) => {
        const routePath = c.routeUrl == undefined ? core_1.NameUtility.camelToKebabCase(c.name) : c.routeUrl; // allow a routeUrl of ''
        angular_1.AngularWriter.writeRoute(tsw, { path: routePath, componentName: `${c.name}Component` });
    });
    tsw.decreaseIndent();
    tsw.writeLine("]");
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LXJlZ2lzdHJhdGlvbi50ZW1wbGF0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbXBvbmVudC1yZWdpc3RyYXRpb24udGVtcGxhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2QkFBNkI7QUFDN0IsMENBQTBEO0FBQzFELHNEQUFrRDtBQUNsRCxzREFBeUQ7QUFDekQsZ0RBQW1EO0FBR25ELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLCtCQUErQixDQUFDLENBQUM7QUFFMUUsc0JBQVMsQ0FBQyxpQkFBaUIsQ0FBVyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsRUFBRSxDQUFDLE1BQWtCLEVBQUUsS0FBZSxFQUFFLEVBQUU7SUFDdkcsTUFBTSxHQUFHLEdBQUcsSUFBSSw2QkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN6QyxNQUFNLGNBQWMsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNELGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7SUFFcEUsZ0JBQWdCO0lBQ2hCLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtRQUMvQixNQUFNLGFBQWEsR0FBRyxrQkFBVyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLGFBQWEsSUFBSSxhQUFhLFlBQVksRUFBRSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3BHLENBQUMsQ0FBQyxDQUFDO0lBQ0gsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBRWhCLGVBQWU7SUFDZixHQUFHLENBQUMsU0FBUyxDQUFDLCtCQUErQixDQUFDLENBQUM7SUFDL0MsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3JCLEdBQUcsQ0FBQyxVQUFVLENBQ2IsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUNoRCxHQUFHLENBQ0gsQ0FBQztJQUNGLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNyQixHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBRW5CLFNBQVM7SUFDVCxHQUFHLENBQUMsU0FBUyxDQUFDLHlCQUF5QixDQUFDLENBQUM7SUFDekMsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBRXJCLEtBQUssQ0FBQyxVQUFVO1NBQ2QsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1NBQzVCLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO1FBQ2QsTUFBTSxTQUFTLEdBQUcsQ0FBQyxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLGtCQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMseUJBQXlCO1FBQ3hILHVCQUFhLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksV0FBVyxFQUFFLENBQUMsQ0FBQztJQUN6RixDQUFDLENBQUMsQ0FBQztJQUVKLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNyQixHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3BCLENBQUMsQ0FBQyxDQUFDIn0=