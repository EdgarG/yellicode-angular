import * as path from "path";
import { NameUtility, TextWriter } from "@yellicode/core";
import { Generator } from "@yellicode/templating";
import { TypeScriptWriter } from "@yellicode/typescript";
import { AngularWriter } from "@yellicode/angular";
import { AppModel } from "./app-model";

const fileName = path.join("../src/app", "app.component-registration.ts");

Generator.generateFromModel<AppModel>({ outputFile: fileName }, (writer: TextWriter, model: AppModel) => {
	const tsw = new TypeScriptWriter(writer);
	const componentNames = model.components.map((c) => c.name);
	componentNames.push('AppSideNav'); // Add our custom side navigation

	// Write imports
	componentNames.forEach((name) => {
		const componentName = NameUtility.camelToKebabCase(name);
		tsw.writeImports(`./components/${componentName}/${componentName}.component`, [`${name}Component`]);
	});
	tsw.writeLine();

	// Declarations
	tsw.writeLine("export const declarations = [");
	tsw.increaseIndent();
	tsw.writeLines(
		componentNames.map((name) => `${name}Component`),
		","
	);
	tsw.decreaseIndent();
	tsw.writeLine("]");

	// Routes
	tsw.writeLine("export const routes = [");
	tsw.increaseIndent();

	model.components
		.filter((c) => c.createRoute)
		.forEach((c) => {
			const routePath = c.routeUrl == undefined ? NameUtility.camelToKebabCase(c.name) : c.routeUrl; // allow a routeUrl of ''
			AngularWriter.writeRoute(tsw, { path: routePath, componentName: `${c.name}Component` });
		});

	tsw.decreaseIndent();
	tsw.writeLine("]");
});
