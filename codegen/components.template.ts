import * as path from "path";
import { NameUtility } from "@yellicode/core";
import { Generator } from "@yellicode/templating";
import { TypeScriptWriter } from "@yellicode/typescript";
import { AppModel, ComponentModel, ParsedComponentConfig } from "./app-model";
import { ComponentClassTemplate } from "./component-class.template";

const componentClassTemplate = new ComponentClassTemplate();

Generator.getModel<AppModel>()
	.then((model) => {
		// Generate a file for each component in the model
		model.components.forEach((component) => generateComponent(component));
	})
	.catch((e) => console.log(e));

function generateComponent(component: ComponentModel) {
	// Let's assume a CamelCase component name in the model.
	// By convention, make it kebab-case for use in file paths.
	const kebabCaseComponentName = NameUtility.camelToKebabCase(component.name);
	const componentDirectory = path.join("../src/app/components", kebabCaseComponentName);
	const fileBaseName = `${kebabCaseComponentName}.component`;
	const fileBasePath = path.join(componentDirectory, fileBaseName);

	const styleType = component.styleType || "css";

	const tsFileName = `${fileBasePath}.ts`;
	const htmlFileName = `${fileBasePath}.html`;
	const styleFileName = `${fileBasePath}.${styleType}`;

	const parsedComponentConfig: ParsedComponentConfig = {
		name: component.name,
		kebabName: kebabCaseComponentName,
		directory: componentDirectory,
		baseName: fileBaseName,
		basePath: fileBasePath,
		tsFileName: tsFileName,
		htmlFileName: htmlFileName,
		templateUrl: `${fileBaseName}.html`,
		styleFileNmae: styleFileName,
		styleUrl: `${fileBaseName}.${styleType}`,
	};

	// 1. Generate the component class
	Generator.generate({ outputFile: tsFileName }, (output) => {
		const tsw = new TypeScriptWriter(output);
		componentClassTemplate.write(tsw, component, parsedComponentConfig);
	});

	// 2. Generate the HTML template
	Generator.generate({ outputFile: htmlFileName }, (output) => {
		output.writeLine(`<!-- HTML template for the '${component.name}' component. -->`);

		// If a title is configured, write it.
		if (component.title) {
			output.writeLine(`<h1>${component.title}</h1>`);
		}
	});

	// 3. Generate the stylesheet
	Generator.generate({ outputFile: styleFileName }, (output) => {
		output.writeLine(`/* Component stylesheet for the '${component.name}' component. */`);
	});
}
