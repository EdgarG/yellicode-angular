"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ComponentClassTemplate = void 0;
const core_1 = require("@yellicode/core");
const angular_1 = require("@yellicode/angular");
const appPaths = require("./app-paths");
const path = require("path");
class ComponentClassTemplate {
    write(writer, model, parsedComponentConfig) {
        const implement = ["OnInit"]; // Whatever interfaces you might want to implement
        const extend = []; // If you have your own component base class, add it here
        const angularCoreImports = ["Component", "OnInit"]; // Add default Angular imports here
        // 1. Imports
        writer.writeImports("@angular/core", angularCoreImports);
        writer.writeLine();
        // Import services that we depend on
        if (model.useServices) {
            model.useServices.forEach(serviceName => {
                const servicePath = path.join(appPaths.servicesDir, `${core_1.NameUtility.camelToKebabCase(serviceName)}.service`);
                writer.writeImports(path.relative(parsedComponentConfig.directory, servicePath), [`${serviceName}Service`]);
            });
        }
        writer.writeLine();
        // 2. Write the component
        const componentConfig = {
            selector: parsedComponentConfig.kebabName,
            templateUrl: `./${parsedComponentConfig.templateUrl}`,
            styleUrls: [`./${parsedComponentConfig.styleUrl}`],
        };
        // 2.1 write the @Component(...) class decorator with the configuration
        angular_1.AngularWriter.writeComponentDecorator(writer, componentConfig);
        // 2.2 write the class itself
        writer.writeClassBlock({
            name: `${model.name}Component`,
            export: true,
            extends: extend,
            implements: implement,
        }, () => {
            // Class constructor
            writer.writeIndent();
            writer.write("constructor(");
            // Inject services
            if (model.useServices) {
                writer.write(model.useServices.map(serviceName => `private ${core_1.NameUtility.upperToLowerCamelCase(serviceName)}Service: ${serviceName}Service`).join(','));
            }
            writer.writeEndOfLine(') {');
            if (extend && extend.length) {
                writer.writeLineIndented("super()");
            }
            writer.writeLine("}");
            writer.writeLine();
            // Class contents
            writer.writeLine("public ngOnInit() {");
            writer.writeLine("}");
        });
    }
}
exports.ComponentClassTemplate = ComponentClassTemplate;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LWNsYXNzLnRlbXBsYXRlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29tcG9uZW50LWNsYXNzLnRlbXBsYXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDBDQUE4QztBQUU5QyxnREFBb0U7QUFFcEUsd0NBQXdDO0FBQ3hDLDZCQUE2QjtBQUU3QixNQUFhLHNCQUFzQjtJQUNsQyxLQUFLLENBQUMsTUFBd0IsRUFBRSxLQUFxQixFQUFFLHFCQUE0QztRQUNsRyxNQUFNLFNBQVMsR0FBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsa0RBQWtEO1FBQzFGLE1BQU0sTUFBTSxHQUFhLEVBQUUsQ0FBQyxDQUFDLHlEQUF5RDtRQUN0RixNQUFNLGtCQUFrQixHQUFhLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsbUNBQW1DO1FBR2pHLGFBQWE7UUFDYixNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3pELE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUVuQixvQ0FBb0M7UUFDcEMsSUFBSSxLQUFLLENBQUMsV0FBVyxFQUFFO1lBQ3RCLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUN0QyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsR0FBRyxrQkFBVyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDNUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzlHLENBQUMsQ0FBQyxDQUFBO1NBQ0E7UUFDRCxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFckIseUJBQXlCO1FBQ3pCLE1BQU0sZUFBZSxHQUFvQjtZQUN4QyxRQUFRLEVBQUUscUJBQXFCLENBQUMsU0FBUztZQUN6QyxXQUFXLEVBQUUsS0FBSyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUU7WUFDckQsU0FBUyxFQUFFLENBQUMsS0FBSyxxQkFBcUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNsRCxDQUFDO1FBRUYsdUVBQXVFO1FBQ3ZFLHVCQUFhLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRS9ELDZCQUE2QjtRQUM3QixNQUFNLENBQUMsZUFBZSxDQUNyQjtZQUNDLElBQUksRUFBRSxHQUFHLEtBQUssQ0FBQyxJQUFJLFdBQVc7WUFDOUIsTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsTUFBTTtZQUNmLFVBQVUsRUFBRSxTQUFTO1NBQ3JCLEVBQ0QsR0FBRyxFQUFFO1lBQ0osb0JBQW9CO1lBQ3BCLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNyQixNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBRTdCLGtCQUFrQjtZQUNsQixJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQ3RCLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLGtCQUFXLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLFlBQVksV0FBVyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUN4SjtZQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFN0IsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtnQkFDNUIsTUFBTSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BDO1lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QixNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFbkIsaUJBQWlCO1lBQ2pCLE1BQU0sQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUN4QyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLENBQUMsQ0FDRCxDQUFDO0lBQ0gsQ0FBQztDQUNEO0FBL0RELHdEQStEQyJ9