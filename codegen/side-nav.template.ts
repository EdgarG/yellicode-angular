import * as path from 'path';
import { NameUtility } from '@yellicode/core';
import { Generator } from '@yellicode/templating';
import { TypeScriptWriter } from '@yellicode/typescript';
import { AngularWriter, ComponentConfig } from '@yellicode/angular';
import { HtmlWriter } from '@yellicode/html';
import { AppModel } from './app-model';

const componentBaseName = 'AppSideNav'; // feel free to make this a TopNav, BottomNav or SomeWhereElseNav...
const kebabCaseComponentName = NameUtility.camelToKebabCase(componentBaseName);
const componentSubDir = path.join('../src/app/components', kebabCaseComponentName);
const fileBaseName = `${kebabCaseComponentName}.component`;

Generator.getModel<AppModel>().then((model: AppModel) => {

  // 1. Generate the component class
  Generator.generate({ outputFile: `${path.join(componentSubDir, fileBaseName)}.ts` }, (output) => {
    const writer = new TypeScriptWriter(output);

    // Import Angular components    
    writer.writeImports('@angular/core', ['Component']);
    writer.writeLine();

    // Write the component class with a @Component decorator
    const componentConfig: ComponentConfig = {
      selector: kebabCaseComponentName,
      templateUrl: `./${fileBaseName}.html`     
    };

    AngularWriter.writeComponentDecorator(writer, componentConfig);    
    writer.writeClassBlock({ name: `${componentBaseName}Component`, export: true }, () => {
      // Nothing here yet
    });
  });

  // 2. Generate the template
  Generator.generate({ outputFile: `${path.join(componentSubDir, fileBaseName)}.html` }, (output) => {
    const writer = new HtmlWriter(output);    
    writer.writeElement('ul', { classNames: 'nav-items', attributes: { style: 'width: 150px;' } }, () => {
      model.components
        .filter(c => c.createRoute && c.showInSideNav)
        .forEach(c => {
          writer.writeElement('li', {}, () => {
            const path = (c.routeUrl == undefined) ? NameUtility.camelToKebabCase(c.name) : c.routeUrl; 
            writer.writeElement('a', { attributes: { routerLink: path } }, c.title || c.name)
          });
        });
    });
  });
});