"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.styleSheetExtension = exports.servicesDir = exports.componentsDir = exports.appDir = void 0;
/**
 * Contains the output paths that are used by code generation templates.
 */
const path = require("path");
exports.appDir = '../src/app';
exports.componentsDir = path.join(exports.appDir, 'components');
exports.servicesDir = path.join(exports.appDir, 'services');
exports.styleSheetExtension = '.css'; // Or change the extension to '.scss', '.less', ...
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXBhdGhzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXBhdGhzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBOztHQUVHO0FBQ0YsNkJBQTZCO0FBRWhCLFFBQUEsTUFBTSxHQUFHLFlBQVksQ0FBQztBQUN0QixRQUFBLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztBQUNoRCxRQUFBLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztBQUM1QyxRQUFBLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxDQUFDLG1EQUFtRCJ9