"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const core_1 = require("@yellicode/core");
const templating_1 = require("@yellicode/templating");
const typescript_1 = require("@yellicode/typescript");
const component_class_template_1 = require("./component-class.template");
const componentClassTemplate = new component_class_template_1.ComponentClassTemplate();
templating_1.Generator.getModel()
    .then((model) => {
    // Generate a file for each component in the model
    model.components.forEach((component) => generateComponent(component));
})
    .catch((e) => console.log(e));
function generateComponent(component) {
    // Let's assume a CamelCase component name in the model.
    // By convention, make it kebab-case for use in file paths.
    const kebabCaseComponentName = core_1.NameUtility.camelToKebabCase(component.name);
    const componentDirectory = path.join("../src/app/components", kebabCaseComponentName);
    const fileBaseName = `${kebabCaseComponentName}.component`;
    const fileBasePath = path.join(componentDirectory, fileBaseName);
    const styleType = component.styleType || "css";
    const tsFileName = `${fileBasePath}.ts`;
    const htmlFileName = `${fileBasePath}.html`;
    const styleFileName = `${fileBasePath}.${styleType}`;
    const parsedComponentConfig = {
        name: component.name,
        kebabName: kebabCaseComponentName,
        directory: componentDirectory,
        baseName: fileBaseName,
        basePath: fileBasePath,
        tsFileName: tsFileName,
        htmlFileName: htmlFileName,
        templateUrl: `${fileBaseName}.html`,
        styleFileNmae: styleFileName,
        styleUrl: `${fileBaseName}.${styleType}`,
    };
    // 1. Generate the component class
    templating_1.Generator.generate({ outputFile: tsFileName }, (output) => {
        const tsw = new typescript_1.TypeScriptWriter(output);
        componentClassTemplate.write(tsw, component, parsedComponentConfig);
    });
    // 2. Generate the HTML template
    templating_1.Generator.generate({ outputFile: htmlFileName }, (output) => {
        output.writeLine(`<!-- HTML template for the '${component.name}' component. -->`);
        // If a title is configured, write it.
        if (component.title) {
            output.writeLine(`<h1>${component.title}</h1>`);
        }
    });
    // 3. Generate the stylesheet
    templating_1.Generator.generate({ outputFile: styleFileName }, (output) => {
        output.writeLine(`/* Component stylesheet for the '${component.name}' component. */`);
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50cy50ZW1wbGF0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbXBvbmVudHMudGVtcGxhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSw2QkFBNkI7QUFDN0IsMENBQThDO0FBQzlDLHNEQUFrRDtBQUNsRCxzREFBeUQ7QUFFekQseUVBQW9FO0FBRXBFLE1BQU0sc0JBQXNCLEdBQUcsSUFBSSxpREFBc0IsRUFBRSxDQUFDO0FBRTVELHNCQUFTLENBQUMsUUFBUSxFQUFZO0tBQzVCLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO0lBQ2Ysa0RBQWtEO0lBQ2xELEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0FBQ3ZFLENBQUMsQ0FBQztLQUNELEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBRS9CLFNBQVMsaUJBQWlCLENBQUMsU0FBeUI7SUFDbkQsd0RBQXdEO0lBQ3hELDJEQUEyRDtJQUMzRCxNQUFNLHNCQUFzQixHQUFHLGtCQUFXLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVFLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0lBQ3RGLE1BQU0sWUFBWSxHQUFHLEdBQUcsc0JBQXNCLFlBQVksQ0FBQztJQUMzRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxDQUFDO0lBRWpFLE1BQU0sU0FBUyxHQUFHLFNBQVMsQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDO0lBRS9DLE1BQU0sVUFBVSxHQUFHLEdBQUcsWUFBWSxLQUFLLENBQUM7SUFDeEMsTUFBTSxZQUFZLEdBQUcsR0FBRyxZQUFZLE9BQU8sQ0FBQztJQUM1QyxNQUFNLGFBQWEsR0FBRyxHQUFHLFlBQVksSUFBSSxTQUFTLEVBQUUsQ0FBQztJQUVyRCxNQUFNLHFCQUFxQixHQUEwQjtRQUNwRCxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUk7UUFDcEIsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxTQUFTLEVBQUUsa0JBQWtCO1FBQzdCLFFBQVEsRUFBRSxZQUFZO1FBQ3RCLFFBQVEsRUFBRSxZQUFZO1FBQ3RCLFVBQVUsRUFBRSxVQUFVO1FBQ3RCLFlBQVksRUFBRSxZQUFZO1FBQzFCLFdBQVcsRUFBRSxHQUFHLFlBQVksT0FBTztRQUNuQyxhQUFhLEVBQUUsYUFBYTtRQUM1QixRQUFRLEVBQUUsR0FBRyxZQUFZLElBQUksU0FBUyxFQUFFO0tBQ3hDLENBQUM7SUFFRixrQ0FBa0M7SUFDbEMsc0JBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUN6RCxNQUFNLEdBQUcsR0FBRyxJQUFJLDZCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixDQUFDLENBQUM7SUFDckUsQ0FBQyxDQUFDLENBQUM7SUFFSCxnQ0FBZ0M7SUFDaEMsc0JBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUMzRCxNQUFNLENBQUMsU0FBUyxDQUFDLCtCQUErQixTQUFTLENBQUMsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDO1FBRWxGLHNDQUFzQztRQUN0QyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEVBQUU7WUFDcEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxPQUFPLFNBQVMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxDQUFDO1NBQ2hEO0lBQ0YsQ0FBQyxDQUFDLENBQUM7SUFFSCw2QkFBNkI7SUFDN0Isc0JBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUM1RCxNQUFNLENBQUMsU0FBUyxDQUFDLG9DQUFvQyxTQUFTLENBQUMsSUFBSSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3ZGLENBQUMsQ0FBQyxDQUFDO0FBQ0osQ0FBQyJ9