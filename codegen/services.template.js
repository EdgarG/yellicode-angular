"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This template generates Angular services. It enumerates all services in the model "app.model.json"
 * and generates a TypeScript class for each service.
 */
const path = require("path");
const core_1 = require("@yellicode/core");
const templating_1 = require("@yellicode/templating");
const typescript_1 = require("@yellicode/typescript");
const appPaths = require("./app-paths");
const service_class_template_1 = require("./service-class.template");
templating_1.Generator.getModel().then((model) => {
    // Generate a file for each service in the model
    model.services.forEach((service) => {
        // Let's assume a CamelCase service name in the model. By convention, make it kebab-case for use in file paths.
        const kebabCaseServiceName = core_1.NameUtility.camelToKebabCase(service.name);
        const fileBaseName = `${kebabCaseServiceName}.service`;
        // Generate the service class
        templating_1.Generator.generate({ outputFile: `${path.join(appPaths.servicesDir, fileBaseName)}.ts` }, (output) => {
            const typeScript = new typescript_1.TypeScriptWriter(output);
            service_class_template_1.writeServiceClass(typeScript, service);
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmljZXMudGVtcGxhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzZXJ2aWNlcy50ZW1wbGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBOzs7R0FHRztBQUNGLDZCQUE2QjtBQUM3QiwwQ0FBOEM7QUFDOUMsc0RBQWtEO0FBQ2xELHNEQUF5RDtBQUV6RCx3Q0FBd0M7QUFDeEMscUVBQTJEO0FBRTNELHNCQUFTLENBQUMsUUFBUSxFQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBZSxFQUFFLEVBQUU7SUFFdEQsZ0RBQWdEO0lBQ2hELEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7UUFDakMsK0dBQStHO1FBQy9HLE1BQU0sb0JBQW9CLEdBQUcsa0JBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEUsTUFBTSxZQUFZLEdBQUcsR0FBRyxvQkFBb0IsVUFBVSxDQUFDO1FBRXZELDZCQUE2QjtRQUM3QixzQkFBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLFVBQVUsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUNuRyxNQUFNLFVBQVUsR0FBRyxJQUFJLDZCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELDBDQUFpQixDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUEifQ==