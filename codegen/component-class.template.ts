import { NameUtility } from "@yellicode/core";
import { TypeScriptWriter } from "@yellicode/typescript";
import { AngularWriter, ComponentConfig } from "@yellicode/angular";
import { ComponentModel, ParsedComponentConfig } from "./app-model";
import * as appPaths from './app-paths';
import * as path from 'path';

export class ComponentClassTemplate {
	write(writer: TypeScriptWriter, model: ComponentModel, parsedComponentConfig: ParsedComponentConfig) {
		const implement: string[] = ["OnInit"]; // Whatever interfaces you might want to implement
		const extend: string[] = []; // If you have your own component base class, add it here
		const angularCoreImports: string[] = ["Component", "OnInit"]; // Add default Angular imports here


		// 1. Imports
		writer.writeImports("@angular/core", angularCoreImports);
		writer.writeLine();

		// Import services that we depend on
		if (model.useServices) {
			model.useServices.forEach(serviceName => {
			  const servicePath = path.join(appPaths.servicesDir, `${NameUtility.camelToKebabCase(serviceName)}.service`);
			  writer.writeImports(path.relative(parsedComponentConfig.directory, servicePath), [`${serviceName}Service`]);
			})
		  }
		  writer.writeLine();

		// 2. Write the component
		const componentConfig: ComponentConfig = {
			selector: parsedComponentConfig.kebabName,
			templateUrl: `./${parsedComponentConfig.templateUrl}`,
			styleUrls: [`./${parsedComponentConfig.styleUrl}`],
		};

		// 2.1 write the @Component(...) class decorator with the configuration
		AngularWriter.writeComponentDecorator(writer, componentConfig);

		// 2.2 write the class itself
		writer.writeClassBlock(
			{
				name: `${model.name}Component`,
				export: true,
				extends: extend,
				implements: implement,
			},
			() => {
				// Class constructor
				writer.writeIndent();
				writer.write("constructor(");

				// Inject services
				if (model.useServices) {
					writer.write(model.useServices.map(serviceName => `private ${NameUtility.upperToLowerCamelCase(serviceName)}Service: ${serviceName}Service`).join(','));
				}
				 
				writer.writeEndOfLine(') {');

				if (extend && extend.length) {
					writer.writeLineIndented("super()");
				}

				writer.writeLine("}");
				writer.writeLine();

				// Class contents
				writer.writeLine("public ngOnInit() {");
				writer.writeLine("}");
			}
		);
	}
}
