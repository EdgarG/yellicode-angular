"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeServiceClass = void 0;
exports.writeServiceClass = (writer, model) => {
    const serviceName = `${model.name}Service`; // The component name from our model + 'Service'
    const angularCoreImports = ['Injectable']; // Add default Angular imports here
    const implement = []; // Whatever interfaces you might want to implement
    const extend = []; // If you have your own service base class, add it here
    writer.writeImports('@angular/core', angularCoreImports);
    writer.writeLine();
    // Injectable class decorator
    writer.writeDecoratorCodeBlock('Injectable', () => {
        writer.writeLine(`providedIn: 'root'`);
    });
    writer.writeClassBlock({ name: serviceName, export: true, extends: extend, implements: implement }, () => {
        // Class constructor
        writer.writeLine(`constructor() {`);
        if (extend && extend.length) {
            writer.writeLineIndented('super()');
        }
        writer.writeLine('}');
        writer.writeLine();
    });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmljZS1jbGFzcy50ZW1wbGF0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNlcnZpY2UtY2xhc3MudGVtcGxhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBUWMsUUFBQSxpQkFBaUIsR0FBRyxDQUFDLE1BQXdCLEVBQUUsS0FBbUIsRUFBRSxFQUFFO0lBQ2pGLE1BQU0sV0FBVyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUUsZ0RBQWdEO0lBQzdGLE1BQU0sa0JBQWtCLEdBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLG1DQUFtQztJQUN4RixNQUFNLFNBQVMsR0FBYSxFQUFFLENBQUMsQ0FBQyxrREFBa0Q7SUFDbEYsTUFBTSxNQUFNLEdBQWEsRUFBRSxDQUFDLENBQUMsdURBQXVEO0lBRXBGLE1BQU0sQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFDekQsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBRW5CLDZCQUE2QjtJQUM3QixNQUFNLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLEdBQUcsRUFBRTtRQUNoRCxNQUFNLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDSCxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRTtRQUN2RyxvQkFBb0I7UUFDcEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3BDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDM0IsTUFBTSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3JDO1FBQ0QsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckIsQ0FBQyxDQUFDLENBQUE7QUFDSixDQUFDLENBQUEifQ==