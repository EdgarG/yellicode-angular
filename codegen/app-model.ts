export interface ServiceModel {
	name: string;
}

export interface ComponentModel {
	name: string;
	title?: string;
	useServices?: string[];
	createRoute?: boolean;
	routeUrl?: string;
	showInSideNav?: boolean;
	styleType?: "css" | "scss" | "less";
}

export interface AppModel {
	services: any[];
	components: ComponentModel[];
}

export interface ParsedComponentConfig {
	name: string;
	kebabName: string;
	directory: string;
	baseName: string;
	basePath: string;
	tsFileName: string;
	htmlFileName: string;
	templateUrl: string;
	styleFileNmae: string;
	styleUrl: string;
}
