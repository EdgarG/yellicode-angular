"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const core_1 = require("@yellicode/core");
const templating_1 = require("@yellicode/templating");
const typescript_1 = require("@yellicode/typescript");
const angular_1 = require("@yellicode/angular");
const html_1 = require("@yellicode/html");
const componentBaseName = 'AppSideNav'; // feel free to make this a TopNav, BottomNav or SomeWhereElseNav...
const kebabCaseComponentName = core_1.NameUtility.camelToKebabCase(componentBaseName);
const componentSubDir = path.join('../src/app/components', kebabCaseComponentName);
const fileBaseName = `${kebabCaseComponentName}.component`;
templating_1.Generator.getModel().then((model) => {
    // 1. Generate the component class
    templating_1.Generator.generate({ outputFile: `${path.join(componentSubDir, fileBaseName)}.ts` }, (output) => {
        const writer = new typescript_1.TypeScriptWriter(output);
        // Import Angular components    
        writer.writeImports('@angular/core', ['Component']);
        writer.writeLine();
        // Write the component class with a @Component decorator
        const componentConfig = {
            selector: kebabCaseComponentName,
            templateUrl: `./${fileBaseName}.html`
        };
        angular_1.AngularWriter.writeComponentDecorator(writer, componentConfig);
        writer.writeClassBlock({ name: `${componentBaseName}Component`, export: true }, () => {
            // Nothing here yet
        });
    });
    // 2. Generate the template
    templating_1.Generator.generate({ outputFile: `${path.join(componentSubDir, fileBaseName)}.html` }, (output) => {
        const writer = new html_1.HtmlWriter(output);
        writer.writeElement('ul', { classNames: 'nav-items', attributes: { style: 'width: 150px;' } }, () => {
            model.components
                .filter(c => c.createRoute && c.showInSideNav)
                .forEach(c => {
                writer.writeElement('li', {}, () => {
                    const path = (c.routeUrl == undefined) ? core_1.NameUtility.camelToKebabCase(c.name) : c.routeUrl;
                    writer.writeElement('a', { attributes: { routerLink: path } }, c.title || c.name);
                });
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZS1uYXYudGVtcGxhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaWRlLW5hdi50ZW1wbGF0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDZCQUE2QjtBQUM3QiwwQ0FBOEM7QUFDOUMsc0RBQWtEO0FBQ2xELHNEQUF5RDtBQUN6RCxnREFBb0U7QUFDcEUsMENBQTZDO0FBRzdDLE1BQU0saUJBQWlCLEdBQUcsWUFBWSxDQUFDLENBQUMsb0VBQW9FO0FBQzVHLE1BQU0sc0JBQXNCLEdBQUcsa0JBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBQy9FLE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztBQUNuRixNQUFNLFlBQVksR0FBRyxHQUFHLHNCQUFzQixZQUFZLENBQUM7QUFFM0Qsc0JBQVMsQ0FBQyxRQUFRLEVBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFlLEVBQUUsRUFBRTtJQUV0RCxrQ0FBa0M7SUFDbEMsc0JBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUM5RixNQUFNLE1BQU0sR0FBRyxJQUFJLDZCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTVDLGdDQUFnQztRQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDcEQsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBRW5CLHdEQUF3RDtRQUN4RCxNQUFNLGVBQWUsR0FBb0I7WUFDdkMsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyxXQUFXLEVBQUUsS0FBSyxZQUFZLE9BQU87U0FDdEMsQ0FBQztRQUVGLHVCQUFhLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQy9ELE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxpQkFBaUIsV0FBVyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUU7WUFDbkYsbUJBQW1CO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFSCwyQkFBMkI7SUFDM0Isc0JBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUNoRyxNQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRTtZQUNsRyxLQUFLLENBQUMsVUFBVTtpQkFDYixNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUM7aUJBQzdDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDWCxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFO29CQUNqQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO29CQUMzRixNQUFNLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUNuRixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIn0=