import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import * as componentRegistration from "./app.component-registration";

// Routes
const routes: Routes = []; // off course, you can add manual routes too
routes.push(...componentRegistration.routes);

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
