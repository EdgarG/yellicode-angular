import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HomeComponent } from './components/home/home.component';
import { AccountComponent } from './components/account/account.component';
import { AppSideNavComponent } from './components/app-side-nav/app-side-nav.component';

import * as componentRegistration from "./app.component-registration";


// Component declarations
let declarations: any[] = [AppComponent,
	AppSideNavComponent]; // add any manual components here
// Add generated components

declarations.push(...componentRegistration.declarations.filter(c => c != AppSideNavComponent));

@NgModule({
	declarations: declarations,
	imports: [BrowserModule, AppRoutingModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
